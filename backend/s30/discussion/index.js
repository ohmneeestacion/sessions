console.log("ES6 Updates!");

// Expoent Operator

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8,2);
console.log(secondNum);

// Template Literals
/*
	- allows us to write strings w/o using concantenation
	- greatly helps us with code readability
*/

let name = "Ken";


// using concatenation
let message = 'Hello ' + name + ' ! Welcome to Programming';
console.log("Message without template literals: " + message);

// using template literals
// backticks (``) and ${} for including js expressions

message = `Hello ${name}! Welcome to Programming`;
console.log(`Message with template literals: ${message}`);

// creates multi-line using template literals

const anotherMessage = `
${name} attended a Math Competition.
He won it by solving the problem 8 ** 2 with the answer of ${firstNum}`;
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings amount is ${principal * interestRate}`);

// Array Destructuring

const fullName = ["Juan", "Dela", "Cruz"];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's Nice to meet you.`);

// using array destructuring

const [firstName, middleName, lastName] = fullName;
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's Nice to meet you.`);

// Object Destructuring
const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
}

// using dot notation
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you.`);

// using object destructuring
const {givenName, familyName, maidenName} = person;

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you.`);


// using object destructuring in functions
function getFullName({givenName, maidenName, familyName}) {
	console.log(`${givenName} ${maidenName} ${familyName}`)
};

getFullName(person);

// Arrow functions
const hello = () => {
	console.log('Hello World!')
}

hello();

// Traditional Functions
// function printFullName (fName, mName, lName) {
// 	console.log(fName+ ' ' + mName + ' ' + lName);
// }

// printFullName('John', 'D', 'Smith');

// Arrow Function with Template Literals
const printFullName = (fName, mName, lName) => {
	console.log(`${fName} ${mName} ${lName}`)
}
printFullName('Jane','D','Smith');

// Arrow Function with Loops
const students = ['John', 'Jane', 'Judy']

// Traditional Function
students.forEach(function(student){
	console.log(`${student} is a student.`)
})

// Arrow Function
students.forEach((student) => {
	console.log(`${student} is a student.`)
})

// Implicit Return Statements

/*Traditional Functions
function add(x, y) {
	let result = x + y;
	return x + y;
}
let total = add(2, 5);
console.log(total);*/

// Arrow Function
// Single Line Arrow Functions
const add = (x, y) => x + y;

let total = add(2, 5);
console.log(total);

// Default Argument values
const greet = (name = 'User') => {
	return `Good Afternoon ${name}`
}

console.log(greet()); // Good Afternoon User
console.log(greet('Judy')); // Good Afternoon Judy

// [SECTION] Class-based Object Blueprints

// Create a class
class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Instatntiate an object

const fordExplorer = new Car();

// Even though the 'fordExploer' object is a const, since it is an object, you may still re-assign values to its properties.
fordExplorer.brand = "Ford";
fordExplorer.name = "Explorer";
fordExplorer.year = 2022;

console.log(fordExplorer);

// This logic applies wether you re-assign the values of each property separately or put it as arguments of the new instance of the class.
const toyotaVios = new Car("Toyota", "Vios", 2018);

console.log(toyotaVios);

const address = ["258", "Washington Ave NW", "California", "90011"];
// Create a variable address with a value of an array containing details of an address.
const [lotNum, streetName, stateName, postalCode] = address;
// Destructure the array and print out a message with the full address using Template Literals.
console.log(`I live at ${lotNum} ${stateName}, ${stateName}, ${postalCode}`);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

// Instatntiate an object

const Baecon = new Dog();

// Even though the 'fordExploer' object is a const, since it is an object, you may still re-assign values to its properties.
Baecon.name = "Baecon";
Baecon.age = 4;
Baecon.breed = "Dachshund";

console.log(Baecon);


const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);