// Server variables for initialization
/*const express = require('express'); // imports express
const app = express(); //initializes express
const port = 4000;

// Middleware
app.use(express.json()); // Registering a middleware that will make express be able to read JSON formatt from requests
app.use(express.urlencoded({extended: true})); // Middleware that will allow express tobe able to read data typees other than the default string and aray it can usually read.
*/
// Server Listener
/*app.listen(port, () => console.log(`Server is runningat port ${port}`));

// [SECTION] Routes 
app.get('/', (request,response) => {
	response.send('Hello World!');
})

app.post('/greeting', (request,response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}`);
})*/

// Mock Database
/*let users = [];
app.post('/register', (request,response) => {
	if(request.body.username !== '' && request.body.password !== '') {
		users.push(request.body)
		response.send(`User ${request.body.username} has been successfully registered!`);
	} else {
		response.send(`Please input BOTH username and password.`);
	}
})
//  Gets the list/array of users
app.get('/users', (request,response) => {
	response.send(users);
})

module.exports = app;*/

// [ACTIVITY]

const express = require('express'); 
const app = express(); 
const port = 4000;


app.use(express.json()); 
app.use(express.urlencoded({extended: true})); 


app.listen(port, () => console.log(`Server is runningat port ${port}`));

app.get('/home', (request,response) => {
	response.send('Welcome to the home page');
})
app.get('/users', (request,response) => {
	response.send(users);
})

let users = [];
app.post('/register', (request,response) => {
	if(request.body.username !== '' && request.body.password !== '') {
		users.push(request.body)
		response.send(`User ${request.body.username} has been successfully registered!`);
	} else {
		response.send(`Please input BOTH username and password.`);
	}
})

app.delete('/delete-user', (request,response) => {
	const { username } = request.body;
  let message = "User not found.";

  if (users.length > 0) {
    for (let i = 0; i < users.length; i++) {
      if (users[i].username === username) {
        users.splice(i, 1); // Remove the user object from the array
        message = `User ${username} has been deleted.`;
        break;
      }
    }
  }

  response.json({ message });
})



module.exports = app;