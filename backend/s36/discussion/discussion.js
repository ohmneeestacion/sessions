// using the aggregate method
db.fruits.aggregate([
  // $match used to match or get documents that satisfies the condition
  // $match is similar to find(). You can use query operators to make your criteria more flexible
  /*
    $match -> Apple, Kiwi, Banana
  */
  { $match: { onSale: true }},
  // $group - allows us to group together documents and create an analysis out of the group elements
  // id: $supplier_id
  /*
    Apple = 1.0
    Kiwi = 1.0
    Banana = 2.0
  */
  { $group: { _id: "$supplier_id", total: { $sum: "$stock"}}}
]);