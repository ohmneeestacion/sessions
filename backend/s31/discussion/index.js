//  JSON Format Example
/*{
	"city": "Pateros",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// [SECTION] JSON Arrays
/*"cities": [
	{
		"city": "Quezon City",
		"province": "Metro Manila",
		"country": "Philippines"
	},
	{
		"city": "Batangas City",
		"province": "Batangas",
		"country": "Philippines"
	},
	{
		"city": "Star City",
		"province": "Pasay",
		"country": "Philippines"
		"rides":[
		{
			"name": "Star Flyer"
		},
		{
			"name": "Gabi ng Lagim"
		}
		]
	}
]*/

// [SECTION] JSON Methods

let zuitt_batches = [
	{	batchName: "303"},
	{	batchName: "271"}
]

// Before stringification, javascript reads tha variable as a regular JS Array
console.log("Output before stringification: ")
console.log(zuitt_batches);


// After the JSON.strinify function, javascript now reads the variable a a string (equivalent to converting the array into JSON format).
console.log("Output after stringification: ")
console.log(JSON.stringify(zuitt_batches));


// User details
/*
let first_name = prompt("What is your first name?");
let last_name = prompt("What is your last name?");
*/

// The stringify function/method converts JS Object/Array into JSON String/format
// let other_data = JSON.stringify({
// 	firstName: first_name,
// 	lastName: last_name
// })

// console.log(other_data);

// [SECTION] Convert Stringfield JSON into Javascript Objects

let other_data_JSON = `[{ "firstName": "Earl", "lastName": "Diaz"}]`;

// The parse function/method converts JSON Stringt into a JS Object/Array
let parsed_other_data = JSON.parse(other_data_JSON);
console.log(parsed_other_data);

// Since the JSON is converted, you can now access the properties in regular javascript fashion.
console.log(parsed_other_data[0].firstName);

let users = `[
    {
      "id": 3,
      "name": "Clementine Bauch",
      "username": "Samantha",
      "email": "Nathan@yesenia.net",
      "address": {
        "street": "Douglas Extension",
        "suite": "Suite 847",
        "city": "McKenziehaven",
        "zipcode": "59590-4157",
        "geo": {
          "lat": "-68.6102",
          "lng": "-47.0653"
        }
      },
      "phone": "1-463-123-4447",
      "website": "ramiro.info",
      "company": 
        "name": "Romaguera-Jacobson",
        "catchPhrase": "Face to face bifurcated interface",
        "bs": "e-enable strategic applications"
      }
    },
    {
      "id": 4,
      "name": "Patricia Lebsack",
      "username": "Karianne",
      "email": "Julianne.OConner@kory.org",
      "address": {
        "street": "Hoeger Mall",
        "suite": "Apt. 692",
        "city": "South Elvis",
        "zipcode": "53919-4257",
        "geo": {
          "lat": "29.4572",
          "lng": "-164.2990"
        }
      },
      "phone": "493-170-9623 x156",
      "website": "kale.biz"
      "company": {
        "name": "Robel-Corkery",
        "catchPhrase": "Multi-tiered zero tolerance productivity",
        "bs": "transition cutting-edge web services"
      }
    }
  ]`;
  
let parsedUsers = JSON.parse(users)
console.log(parsedUsers);


//Objective 2:

let product;

//Stringify an object with the following properties and save it in the given stringifiedProduct variable:
let stringifiedProduct;






//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        parsedUsers: typeof parsedUsers !== 'undefined' ? parsedUsers : null,
        product: typeof product !== 'undefined' ? product : null,
        stringifiedProduct: typeof stringifiedProduct !== 'undefined' ? stringifiedProduct : null

    }
} catch(err){

}