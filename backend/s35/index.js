// Greater than operator
db.users.find({
	age: {
		$gte: 82
	}
})

// less than operator
db.users.find({
	age: {
		$lte: 82
	}
})

// Regex operator
db.users.find({
	lastName: {
		$regex: 's'
	}
})


// Combining Operators
db.users.find({
	age: {
		$gt: 70
	},
	lastName: {
		$regex: 'g'
	}
})

db.users.find({
	{
	"_id": 0,
	"firstName": 1		
	},
	firstName:{
		$regex: 'j',
		$options: 'i'
	}
})

// Field Projection
db.users.find({
	lastName:{
		$regex: 'd',
		$options: 'i'
	}
}, 
{
	"_id": 0,
	"lastName": 1
})
