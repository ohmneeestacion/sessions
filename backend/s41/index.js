// Server variables
const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config();
const app = express();
const port = 4000;

// MongoDB Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@303-estacion.lodgddk.mongodb.net/B303-todo?retryWrites=true&w=majority`,{ useNewUrlParser: true,
	useUnifiedTopology: true
});

let database = mongoose.connection;

database.on('error', () => console.log('Connection error : '));
database.once('open', () => console.log('Connected to MongoDB!'));


// Middleware
app.use(express.json())
app.use(express.urlencoded({extended: true}));


// [SECTION] Mongoose Schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
	default: "pending"
	}
});

// [SECTION] Models
const Task = mongoose.model("Task", taskSchema);

// [SECTION] Routes
app.post('/tasks', (request,response) => {
	Task.findOne({name: request.body.name}).then((result, error) => {
		if(result != null && result.name == request.body.name){
			return response.send("Duplicate task found");
		} else {
			let newTask = new Task ({
				name: request.body.name
			})

			newTask.save().then((savedTask, error) => {
				if(error){
					return response.send({
						message: error.message
					});
				}

				return response.send(201, 'New task created!');
			})
		}
	})
})

// Get all tasks
app.get('/tasks', (request, response) => {
	Task.find({}).then((result, error) => {
		if(error){
			return response.send({
				message: error.message
			})
		}
		return response.status(200).json({task: result
		})
	})
})

module.exports = app


/*// [ACTIVITY]
const express = require("express");
const mongoose = require("mongoose");
require("dotenv").config();
const app = express();
const port = 4000;

mongoose.connect(
  `mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@303-estacion.lodgddk.mongodb.net/B303-todo?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

mongoose.connection.on("error", () => console.log("Connection error :("));
mongoose.connection.once("open", () => console.log("Connected to MongoDB!"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.listen(port, () => console.log(`Server is running at port ${port}`));

// Create the User schema
const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
    trim: true,
  },
  password: {
    type: String,
    required: true,
    trim: true,
  },
  status: {
    type: String,
    default: "pending"
  }
});

// Create the User model
const User = mongoose.model("User", userSchema);

app.post('/users', (request, response) => {
  //response.send(request.body);
  User.findOne({username: request.body.username}).then((result) => {
    if(result != null && result.username == request.body.username) {
      return response.send("Duplicate username found");
    } 
    else {
      //Creates new instance
      if((request.body.username !== "" || request.body.username !== undefined || request.body.username !== null) && (request.body.password !== "" || request.body.password !== undefined || request.body.password !== null)){
        let newUser = new User({
          username: request.body.username,
          password: request.body.password
        });
        newUser.save().then((savedUser, error) => {
          if(error) return response.send({message: error.message})
          return response.send(201, 'New User Created');
        })
      }
      else{
        return response.send("Both username and password must be provided");
      }
    }}
  )
})

module.exports = app;*/
