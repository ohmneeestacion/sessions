// Javascript consists of whats called 'statements'. And statements are basically just synta with semi-colons at the end.
// alert("Hello World!");


// Javascript can access the 'log' function of the console to display text/data in the console.
console.log("Hello World!");

// [SECTION] Variables

let my_variable = "Hola, Mundo!";
console.log(my_variable);

// Concatenating Strings
let country = 'Philippines';
let province = 'Metro Manila';

let full_address = province + ", " + country;

console.log(full_address);

// Numbers/Integers
let headcount =26;
let grade = 98.7;

console.log("The number of student is " + headcount + " and the average grade of all is " + grade)

// let sum = headcount + grade;

// console.log(sum);

// Boolean - The value of Boolean is ONLY True or False, When naming variables that have a boolean value, make sure they're formatted like a question.

let is_married = false;
let is_good_conduct = true;

console.log("He's married: " + is_married);
console.log("She's a good person: " + is_good_conduct);

// Arrays
let grades = [98.7, 89.9, 90.2, 94.6];
let details = ["John", "Smith", 32, true];

console.log(grades + details);

// Objects

let person = {
	fullName: "Juan Dela Cruz",
	age: 40,
	isMarried: false,
	contact: ["09992223313", "09448876691"],
	address: {
		houseNumber: "345",
		city: "England"
	}
}

// Javascript reads arrays as objects. This is mainly to accomodate for specific functionalities that arrays can do later on.
console.log(typeof person);
console.log(typeof grades);

// Null & Undefined
let girlfriend = null;
let full_name;

console.log(girlfriend);
console.log(full_name);


// Arithmetic Operators
let first_number = 6;
let second_number = 5;

let sum = first_number + second_number;
let difference = first_number - second_number;
let product = first_number * second_number;
let quotient = first_number / second_number;
let remainder = first_number % second_number;

console.log("Sum: " + sum);
console.log("Difference: " + difference);
console.log("Product: " + product);
console.log("Quotient: " + quotient);
console.log("Remainder: " + remainder);

// Assignment Operators

// initialize 0 as assignment number
let assignment_number = 0;

// 
assignment_number = assignment_number + 2;
console.log("Result of addition assignment operator: " +assignment_number);

assignment_number += 2;
assignment_number++;
console.log("Result of shorthand addition assignment operator: " + assignment_number);