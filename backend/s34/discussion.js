db.users.insertMany([
	{
		"firstName": "John",
		"lastName": "Doe",
	},
	{
		"firstName": "Joseph",
		"lastName": "Doe",
	}
]);


// [SECTION] Retrieving documents
// Retriving all the inserted users
db.users.find();


// Retrieving a specific document from a collection
db.users.find({"firstName": "John" });


// [SECTION] Updating existing documents
db.users.updateOne(
    {
        "_id": ObjectId("64c1c2bbb234912a4e4d5d7c") 
    },
    {
        $set: {
            "lastName": "Gaza"
        }
    }
);

// For updating multiple documents
db.users.updateOne(
	{
		"lastName": "Doe"
	},
	{
		$set: {
			"firstName": "Mary"
		}
	}
);

// [SECTION] Deleting documents from a collection
// Deleting multiple documents
db.user.deleteOne({
	"lastName": "Doe"
});

// [ACTIVITY TEMPLATE]

In the addManyFunc(), copy and paste your query to insert multiple rooms (insertMany method)  in the rooms collection with the following details:
name - double
accommodates - 3
price - 2000
description - A room fit for a small family going on a vacation
rooms_available - 5
isAvailable - false


db.rooms.insertMany([
	{
		name: "double",
		accomodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
])

