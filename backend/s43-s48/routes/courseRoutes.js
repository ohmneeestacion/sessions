const express = require('express');
const router = express.Router();
const auth = require('../auth.js');
const CourseController = require('../controllers/CourseController.js');

// You can destructure the auth variable to extract the function being exported from it. You can then use the functions directly without having to use dot(.) notation.
// const {verify, verifyAdmin} = auth;


// Insert Routes here

// Create single post
router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.addCourse(request, response)
});

// Get all courses
router.get('/all', (request, response) => {
	CourseController.getAllCourses(request, response);
});

// Get all active courses
router.get('/', (request, response) => {
	CourseController.getAllActiveCourses(request, response);
})

// Get a single course
router.get('/:id', (request,response) => {
	CourseController.getCourse(request, response);
})
// update course by id
router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.updateCourse(request, response);
})
// archive post
router.put(':id/archive', auth.verify, (request, response) => {
	CourseController.archiveCourse(request, response);
})
// activate post
router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.activateCourse(request, response);
})

module.exports = router;