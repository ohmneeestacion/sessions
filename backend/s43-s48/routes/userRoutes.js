const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

// Check if email exits
router.post('/check-email', (request, response) => {
	// Controller function goes here
	UserController.checkEmailExists(request.body).then((result) => {
		response.send(result);
	})
;})
	
// Register user
router.post('/register', (request, response) => {
	UserController.registerUser(request.body).then((result) => {
		response.send(result);
	})
})

// Login user
router.post('/login', (request, response) => {
	UserController.loginUser(request, response);
})

// Get user details
router.post('/details', auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.getProfile(request.body).then((result) => {
		response.send(result);
	})
})

router.post('/enroll', auth.verify, (request, response) => {
	UserController.enroll(request, response);
})

// get user enrollments
router.get('/enrollments', auth.verify, (request, response) => {
	UserController.getEnrollments(request, response);
})

// search
router.get('/search', (request, response) => {
	UserController.searchCourses(request, response);
})

module.exports = router;