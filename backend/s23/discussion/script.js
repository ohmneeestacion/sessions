






































//  Ternary Operators
let result = (1 < 10) ? true : false;

console.log("Data returned from the ternary operator is " + result);

// This is the if-else equivalent of the ternary operation
// if (1 < 10) {
// 	return true;
// }
// else {
// 	return false;
// }

// If there are multiple lines within the if-else

if (5 ==5){
	let greeting = "hello";
	console.log(greeting);
}

console.log("Value returned from the ternary operator is ");


// [SECTION] Switch Statements
let day = prompt("What day of the week is it today?"). toLowerCase();

switch(day){
case 'monday':
	console.log("The day today is monday!");
	break;
case 'tuesday':
	console.log("The day today is tuesday!");
	break;
case 'wednesday':
	console.log("The day today is wednesday!");
	break;
case 'thursday':
	console.log("The day today is thursday!");
	break;
case 'friday':
	console.log("The day today is friday!");
	break;
default:
	console.log("Please input a valid day naman paareh");
	break;
}

// [SECTION] Try/Catch/Finally Statements
function showIntensityAlert(windSpeed){
	try{
		alerat(determineTyphoonIntensity(windSpeed));
	} catch(error){
		console.log(error.message)
	} finally {
		alert("Intensity updates will show new alert!");
	}
}

showIntensityAlert(56);