console.log('Mutator Methods')

// Iteration Methods

// - loops through all the elements to perform repatitive tasks on the array

// forEach() - to loop through the array
// map() - loops
// filter() - returns a new array containing elements which meets the given condition

// every() - check if all elements meet the given condition
// return true if all elements meet the given condition, however, false if its does not.

let numbers = [1, 2, 3, 4, 5, 6];

let allValid = numbers.every(function(number) {
		return number > 3;
})
	
console.log("Result of every method: ")
console.log(allValid)

// some() checks if atleast one eement meets the given condition

let someValid = numbers.some(function(number){
	return number < 0;
})

console.log("Result of some method: ")
console.log(someValid)

// includes() method - methods can be "chanined" using them one after another

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product) {
	return product.toLowerCase().includes('a')
})

console.log("Result of includes method: ")
console.log(filteredProducts)


// reduce
let iteration = 0;
let reducedArray = numbers.reduce(function(x,y) {

	console.warn('current iteration: '+ ++iteration);
	console.log('accumulator: '+ x);
	console.log('currentValue: '+ y);

	return x+y;
})

console.log("Result of reduce method: " + reducedArray);

let productsReduce = products.reduce(function(x, y) {
	return x + ' ' + y;
})
console.log("Result of reduce method: " + [productsReduce])