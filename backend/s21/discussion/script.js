// FUNCTION DECALARATION AND INVOKECATION
function printName(){
	console.log("My name is Jeff");
}

printName();

// FUNCTION EXPRESSION
let variable_function = function(){
	console.log("Hello from function expression!")
}

variable_function()

// SCOPING
let global_variable = "Call me mr. worldwide";

console.log(global_variable);

function showNames(){
	let function_variable = "Joe";
	const function_const = "John";

	console.log(function_variable);
	console.log(function_const);

	// You CAN use global variables inside any functions as long as they are declared outside of the function scope.
	console.log(global_variable);
}

	// You CANNOT use locally-scoped variable outside the function they are declared in.

	// console.log(function_variable);

showNames();

// NESTED FUNCTIONS

function parentFunction(){
	let name = "Jane";

	function childFunction(){
		let nested_name = "John";

		// Accessing the 'nested_name' variable outside the function it was declared in will not work
		console.log(name);
		console.log(nested_name);
	}

	childFunction();
	// Accessing the 'nested_name' variable outside the function it was declared in will not work
	// console.log(nested_name);
}

parentFunction();

// BEST PRACTICE FOR FUNCTION NAMING
function printWelcomeMessageForUser(){
	let first_name = prompt('Enter your first name: ', '');
	let last_name = prompt("Enter you last name: ");

	console.log("Hello, " + first_name + " " + last_name + "!");
	console.log("Welcome sa page ko!");
}
printWelcomeMessageForUser();

// RETURN STATEMENT
function fullName(){
	return "Ohmz";
}

console.log(fullName());