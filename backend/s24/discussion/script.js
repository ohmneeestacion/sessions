// [SECTION] While Loop
// let count = 5;

// while (count !== 0) {
// 	console.log("Current value of count: " + count);
// 	count--;
// }

// // [SECTION] Do - While Loop
// let number = Number(prompt("Give ne a number: "));

// do{
// 	console.log("Current value of number: " + number);

// 	number += 1;
// } while (number <= 10)

// // [SECTION] For Loop
// for(let count = 0; count <= 20; count++){
// 	console.log("Current for loop value: " + count);
// }

// let my_string = "earl";

// // To get the length of a string
// console.log(my_string.length);

// // To get a specific letter in a string
// console.log(my_string[2]);

// // Loops through each letter in the string and will keep iterating as long as the current index is less than the length of the string.
// for(let index = 0; index < my_string.length; index++){
// 	console.log(my_string[index]); 
// 	// starts at letter 'e' and will keep printing each etter up to 'L'.
// }

// MINI Activity (20mins.)
// 1. Loop through the 'my_name' variable which has a string with your name on it
// 2. Display each letter in the console but exclude all the vowels

// let my_name = "Ohmnee Estacion"

// for(let index = 0; index < my_name.length; index++){
// 	if (my_name[index].toLowerCase() != "o" && my_name[index].toLowerCase() != "e" && my_name[index].toLowerCase() != "i" )
// 	console.log(my_name[index]); 
// }	


// Another version
// inputStringName = prompt("Let's filter vowels on your name, Enter you name here: ");
// function excludeVowels(inputStringName) {
//   return inputStringName.replace(/[aeiouAEIOU]/g, '');
// }
// const result = excludeVowels(inputStringName);
// console.log(result);

// for(let index = 0; index < result.length; index++){
// 	console.log(result[index]);
// }

// console.log("Hello World");


//Objective 1
//Add code here

let value = prompt("Provide a number");
console.log("The number you provided is " + value + ".");

//using for loop
    for(let count = value; count >= 0; count--){
        if (count <= 50){
            break;
        }
        else (count % 10 === 0){
            console.log("Skipping " + count + " because it is divisible by 10.");
            continue;
        }
        else (count % 5 === 0) {
        	console.log(count);
        }
    }






//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here





//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null

    }
} catch(err){

}