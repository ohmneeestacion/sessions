const Task = require('../models/Task.js');

module.exports.getAllTasks = () => {
	return Task.find({}).then((result, error) => {
		if(error){
			return response.send({
				message: error.message
			});
		};

		return {
			task: result
		}
	})
}

module.exports.createTask = (request_body) => {
	return Task.findOne({name: request_body.name}).then((result, error) => {
		if(result != null && result.name == request_body.name){
			return {
				message: "Duplicate user found!"
		};
		} else {
			let newTask = new Task ({
				name: request_body.name
			})

			return newTask.save().then((savedTask, error) => {
				if(error){
					return {
						message: error.message
					};
				}

				return {
					message: 'New tasks created!'
				};
			})
		}
	})
}
module.exports.getSpecificTask = (request) => {
    return Task.find({_id: request.params.id}).then((result, error) => {
        if(error) return {status: error.status, message: error.message};
        return {status: 200, message: result};
    });
}