import CourseCard from '../components/CourseCard.js';
import { useEffect, useState } from 'react';

export default function Courses(){
	const [courses, setCourses] = useState([]);

	// The useEffect hook will run everytime the Courses page loads, which will then retrieve all courses from the API and set it to their specific CourseCards.
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/api/courses`)
		.then(response => response.json())
		.then(result => {
			setCourses(result.map(course => {
				return (
					<CourseCard key={course._id} course={course}/>
				)
			}))
		})
	}, [])

	return(
		<>
			<h1>Courses</h1>
			{ courses }
		</>
	)
}
