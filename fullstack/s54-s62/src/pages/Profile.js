import React from 'react';
import { Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext.js';
import {Navigate} from 'react-router-dom';
import { useContext, useState, useEffect } from 'react';

export default function Profile(){
  const {user} = useContext(UserContext);
  const [details, setDetails] = useState({})

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        id: user.id
      })
    })
    .then(response => response.json())
    .then(result => {
      if(typeof result._id !== 'undefined'){
        setDetails(result)
      }
    })
  })

  return (
    (user.id === null) ?
      <Navigate to='*'/>
    :
    <div className="p-5 text-center">
      <h2 className="my-5">Profile</h2>
      <Row>
        <Col>
        <h1>{`${details.firstName} ${details.lastName}`}</h1>
        </Col>  
        <Col>
        <h4 className="my-5">Contact Information</h4>
        <ul>
        <li>Email: {details.email} </li>
        <li>Mobile Number: {details.mobileNo} </li>
        </ul>
        </Col>
      </Row>
    </div>
  );
};
